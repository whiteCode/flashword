Recommended Visual Studio Code settings

{
    "window.zoomLevel": 1,
    "typescript.autoImportSuggestions.enabled": true,
    "typescript.referencesCodeLens.enabled": true,
    "typescript.implementationsCodeLens.enabled": true,
    "editor.codeLens": true,
    "tslint.autoFixOnSave": true
}
