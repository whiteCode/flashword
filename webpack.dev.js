var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var urls = require('./src/config/urls.js')
let FaviconsWebpackPlugin = require('favicons-webpack-plugin')

module.exports = {
    context: __dirname,
    entry: [
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://localhost:9966',
        'webpack/hot/only-dev-server',
        path.resolve(__dirname, 'src', 'index.tsx')
    ],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist', 'js'),
        publicPath: '/'
    },
    devtool: 'source-map',
    devServer: {
        hot: true,
        port: 9000,
        contentBase: path.resolve(__dirname, 'dist'),
        publicPath: '/',
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader'
            },
            {
                test: /\.tsx?$/,
                use: ['react-hot-loader/webpack', 'awesome-typescript-loader'],
                exclude: /node_modules/
            },
            { test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
        alias: {
            'src': path.resolve('src/'),
        },
    },
    plugins: [
        new CopyWebpackPlugin([{ from: path.resolve(__dirname, 'index.html') }]),
        new webpack.DefinePlugin({
            APIURL: JSON.stringify(urls[process.env.NODE_ENV]['api']),
            TRANSLATEURL: JSON.stringify(urls[process.env.NODE_ENV]['translate'])
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new FaviconsWebpackPlugin('src/assets/favicon.png')        
    ]
};
