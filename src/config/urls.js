const urls = {
    production: {
        translate: 'https://translate.flashword.a1t.pl',
        api: 'https://api.flashword.a1t.pl',
    },
    development: {
        translate: 'http://localhost:8080',
        api: 'http://localhost:8090',
    }
}

module.exports = urls
