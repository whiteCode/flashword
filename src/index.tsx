import * as React from 'react'
import * as ReactDOM from 'react-dom'

import createHistory from 'history/createBrowserHistory'
import App from 'src/features/App/App'

//@ts-ignore
const history = createHistory()

ReactDOM.render(<App />, document.getElementById('app'))
