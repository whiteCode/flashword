import 'jest'
import { shallow } from 'enzyme'
import * as React from 'react'

import TextPage from './TextPage'

it('renders correctly', () => {
    const component = shallow(<TextPage />)
    expect(component).toMatchSnapshot()
})

// todo add test with childrens
