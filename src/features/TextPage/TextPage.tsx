import * as React from 'react'

import styles from './styles'

export default class TextPage extends React.PureComponent {
    render() {
        return <section className={styles.container}>
            <div className={styles.text}>
                {this.props.children}
            </div>
        </section>
    }
}

