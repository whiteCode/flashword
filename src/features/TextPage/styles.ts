import { style, media } from 'typestyle'
import { rem, percent, px } from 'csx'

const container = style({
    $debugName: 'container',
    color: 'rgba(15, 15, 0, 0.71)',
    padding: `${rem(0)} ${rem(1)}`,
    flexGrow: 1,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'normal',
    height: percent(95),
    $nest: {
        'header': {
            fontSize: rem(3),
            fontWeight: 400,
            lineHeight: rem(3.2),
            wordSpacing: rem(0.6),
            fontFamily: 'Muli, sans-serif',
        },
        'p': {
            fontFamily: 'Playfair Display, serif',
            marginTop: rem(1.2),
            fontSize: rem(2),
            lineHeight: rem(2.2),
            textAlign: 'left',
            opacity: 0.8,
        },
    },
},
    media({ minWidth: 1024 }, {
        maxWidth: px(500),
    }),
)

const text = style({
    alignItems: 'flex-end',
    justifyContent: 'end',
    textAlign: 'right',
    userSelect: 'none',
    '-webkit-touch-callout': 'none',
})

export default { container, text }
