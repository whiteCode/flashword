import * as React from 'react'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import { observer } from 'mobx-react'
import { action } from 'mobx'

import TextPage from 'src/features/TextPage/TextPage'
import TranslateWord from 'src/features/TranslateWord/TranslateWord'
import { List, IListProps } from 'src/features/List/List'
import { VisibleListSection } from 'src/features/List/model'
import { MobileLayout, DesktopLayout, ILayoutProps } from 'src/features/Layout/Layout'
import AppStore from './AppStore'
import UserInfo from 'src/features/UserProfile/UserInfo/UserInfo'
import Navigation from 'src/features/SidePanel/Navigation/Navigation'
import Login from 'src/features/UserProfile/Login/Login'
import { Footer } from 'src/features/SidePanel/Footer/Footer'
import styles from './styles'
import { ToastContainer } from 'react-toastify'
import { VisibleSection } from 'src/features/Layout/model'
import EmptyPage from 'src/features/EmptyPage/EmptyPage'
import Filters from 'src/features/List/Filters'
import Details from 'src/features/Details/Details'


const app = new AppStore()

@observer
export default class App extends React.Component {
    renderAboutPage() {
        return <div className={styles.green}>
            <header> Meet The Words </header>
            <p>
                By FlashWords a simple app created
                to gather checked words on an easily accessible list.
                Now always available in your hands.
            </p>
        </div >
    }

    renderExportsPage() {
        return <div className={styles.blue}>
            <header> Remember Forever </header>
            <p>
                You working on knowledge. We working on storage.
                Of course, it would be nice to have some backup.
                Here you can export your words. </p>
        </div>
    }

    renderTestsPage() {
        return <div className={styles.pink}>
            <header>Dictation Exam</header>
            <p>
                Test your knowledge with FlashWords
                It's time to beat new words.
                This feature is still under development.
            </p>
        </div>
    }

    @action.bound
    onSectionChanged(index: VisibleSection) {
        app.visibleSection = index
    }

    render() {
        const { translateWord, userProfile, list, visibleSection, deviceType } = app
        const { word } = translateWord
        const onSubmit = translateWord.onSubmit
        const layoutProps: ILayoutProps = {
            visibleSection,
            onSectionChanged: this.onSectionChanged,
            navigation: <Navigation
                onRouteChanged={this.onSectionChanged}
                login={(userProfile.profile) ? <div /> : <Login onSetProfile={userProfile.onSetProfile} />}
            />,
            header: userProfile.profile && (
                <UserInfo name={userProfile.profile.name} picture={userProfile.profile.picture} />
            ),
            footer: <Footer />,
        }

        const listProps: IListProps = {
            list: list.list,
            details: list.details,
            onChangeSection: list.onChangeSection,
            onShowDetails: list.onShowDetails,
            visibleSection: list.visibleSection,
            deviceType: app.deviceType,
        }
        const listContainerProps = { // todo fix type
            visibleSection: list.visibleSection === VisibleListSection.Details ? 1 : list.visibleSection, // todo get rid of this
            onSectionChange: list.onChangeSection,
        }
        const Layout = app.Layout
        const SwipeableColumnsLayout = app.SwipeableColumnsLayout
        return (
            <Router>
                <main className={styles.main}>
                    <Layout {...layoutProps}>
                        <div className={styles.routesContainer}>
                            <Route exact
                                path="/"
                                render={() =>
                                    <SwipeableColumnsLayout>
                                        <TextPage > {this.renderAboutPage()}</TextPage>
                                        <EmptyPage />
                                    </SwipeableColumnsLayout>} />
                            <Route exact
                                path="/about"
                                render={() =>
                                    <SwipeableColumnsLayout>
                                        <TextPage > {this.renderAboutPage()}</TextPage>
                                        <EmptyPage />
                                    </SwipeableColumnsLayout>} />
                            <Route exact
                                path="/tests"
                                render={() =>
                                    <SwipeableColumnsLayout>
                                        <TextPage > {this.renderTestsPage()}</TextPage>
                                        <EmptyPage />
                                    </SwipeableColumnsLayout>
                                } />
                            <Route exact
                                path="/exports"
                                render={() =>
                                    <SwipeableColumnsLayout>
                                        <TextPage > {this.renderExportsPage()}</TextPage>
                                        <EmptyPage />
                                    </SwipeableColumnsLayout>} />

                            <Route
                                path="/translate"
                                render={() => (
                                    <SwipeableColumnsLayout>
                                        <TranslateWord
                                            word={word}
                                            onSubmit={onSubmit}
                                            deviceType={deviceType}
                                        />
                                        <EmptyPage />
                                    </SwipeableColumnsLayout>

                                )}
                            />
                            <Route
                                path="/list"
                                render={() =>
                                    <SwipeableColumnsLayout {...listContainerProps}>
                                        <List {...listProps} />
                                        {list.visibleSection === VisibleListSection.Filters
                                            ? <Filters />
                                            : list.visibleSection === VisibleListSection.Details
                                                ? <Details
                                                    store={list.details}
                                                    deviceType={deviceType} />
                                                : <div> placeholder </div>
                                        }
                                    </SwipeableColumnsLayout>
                                }
                            />
                        </div>
                    </Layout>
                    <ToastContainer />
                </main>
            </Router >
        )

    }
}
