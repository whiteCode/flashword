import 'jest'

import AppStore from './AppStore'

describe('app store general test', () => {
    it('initialize properly', () => {
        const store = new AppStore()
        expect(store).toBeInstanceOf(AppStore)
    })
})
