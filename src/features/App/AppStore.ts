
import { observable } from 'mobx'
import { toast } from 'react-toastify'

import TranslateWordStore from 'src/features/TranslateWord/TranslateWordStore'
import { client } from 'src/features/Api/api'
import UserProfileStore from 'src/features/UserProfile/UserProfileStore'
import { VisibleSection } from 'src/features/Layout/model'
import ListStore from 'src/features/List/ListStore'
import {ICommon} from './model'
import { getDeviceType, DeviceSpecific } from 'src/features/DeviceSpecificLayout/index'
import { MobileLayout, DesktopLayout } from 'src/features/Layout/Layout'
import { Mobile, Desktop } from 'src/features/SwipeableColumnsLayout/SwipeableColumnsLayout'


export default class AppStore {
    
    onError(message: string) {
        toast.error(message)
    }

    onSuccess(message: string) {
    // toast.success(message)
    }

    api = client({ handleErrors: this.onError, handleSuccess: this.onSuccess })

    userProfile: UserProfileStore = new UserProfileStore()

    common: ICommon = { getUserID: () => this.userProfile.id }

    list: ListStore = new ListStore({
        common: this.common,        
        api: {
            getList: this.api.getList, 
            getDetails: this.api.getDetails, 
            deleteWord: this.api.deleteWord,
        },
    })

    translateWord: TranslateWordStore = new TranslateWordStore({
        common: this.common,
        api: this.api,
        onSaveNewPair: this.list.fetchData,
    })

    @observable visibleSection: VisibleSection = 0

    deviceType = getDeviceType()

    Layout = DeviceSpecific(MobileLayout,
        DesktopLayout, this.deviceType) 

    SwipeableColumnsLayout = DeviceSpecific(Mobile, Desktop, this.deviceType)
}
