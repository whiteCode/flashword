import { style, media } from 'typestyle'
import { percent, px, rem } from 'csx'

import colors from 'src/common/Colors'

const max = 100

const main = style({
    $debugName: 'main',
    flex: 1,
    width: percent(max),
    height: percent(max),
    margin: px(0),
})

const green = style({
    color: colors.green.toString(),
})

const blue = style({
    color: colors.blue.toString(),
})

const pink = style({
    color: colors.pink.toString(),
})

const routesContainer = style({
    $debugName: 'routesContainer',
    height: `calc(${percent(100)} - ${rem(2)})`,
    flexGrow: 1,
    margin: rem(1),
    overflow: 'hidden',
    width:  `calc(${percent(100)} - ${rem(8)})`,
}, media({ maxWidth: 1024 }, {
    height: `calc(${percent(100)} - ${rem(2)})`,
    width: `calc(${percent(100)} - ${rem(2)})`,
    $nest: {
        'section': {
            padding: rem(1),
            height: `calc(${percent(100)} - ${rem(2)})`,
        },
    },
},

))

export default {
    main,
    green,
    blue,
    pink,
    routesContainer,
}
