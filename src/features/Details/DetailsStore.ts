import { observable, computed } from 'mobx'
import { IDetailsApi } from 'src/features/Details/model'
import { promisedComputed } from 'computed-async-mobx'
import { ICommon } from '../App/model'
import { IDetails, IGetDetailsResponses } from 'src/features/Api/model'

interface IDetailsStore {
    api: IDetailsApi
    common: ICommon
    onDeleteWordSuccess: () => void
}

export default class DetailsStore {
    @observable origin: string = ''

    constructor(private props: IDetailsStore) {

    }

    private wordRequest = promisedComputed(undefined, async () => {
        return this.origin
            ? this.props.api.getDetails(this.origin)
                .then(response => response)
                .catch(error => undefined)
            : undefined
    })

    @computed get word():  undefined | IDetails {
        const word = this.wordRequest.get()
        return word && word
    }

    @computed get isBusy(): boolean {
        return this.wordRequest.busy
    }

    deleteWord() {
        const id = this.props.common.getUserID()
        if (id) {
            this.props.api.deleteWord({ id, word: this.origin }).then(response => {
                this.onDeleteWordSuccess()
            })
        }
    }

    private onDeleteWordSuccess() {
        this.props.onDeleteWordSuccess()
        this.origin = ''
    }
}
