import { GetWordDetails, DeleteWord } from 'src/features/Api/model'

export interface IDetailsApi {
    getDetails: GetWordDetails
    deleteWord: DeleteWord
}
