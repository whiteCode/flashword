import * as React from 'react'
import { observer } from 'mobx-react'
import autobind from 'autobind'
import SwipeableViews from 'react-swipeable-views'

import styles from './Details.styles'
import DetailsStore from './DetailsStore'
import HeaderAction from 'src/features/List/HeaderAction'
import WordDetail from 'src/features/TranslateWord/WordDetail'
import { Device } from 'src/features/DeviceSpecificLayout'
import { IDetails } from '../Api/model'

interface IDetailsProps {
    store: DetailsStore // replace with interface
    deviceType: Device
}

@observer
@autobind
export default class Details extends React.Component<IDetailsProps> {
    onRemoveHandler() {
        if (confirm('Are you sure you want to remove this word from list?')) {
            this.props.store.deleteWord()
        }
    }

    renderDetails(word:  IDetails, isBusy: boolean) {
        const { deviceType } = this.props
        console.log('method')

        return deviceType === Device.Mobile
            ?<div className={styles.swipeableWrapper}> 
             <SwipeableViews style={styles.swipeableContainer} >
                <div className={styles.responsesWrapper}>
                    <legend>Origin:</legend>
                    <p>{word.origin} </p>
                    <WordDetail
                        label={'Translated'}
                        content={word.translated}
                        isBusy={isBusy} />
                    <WordDetail
                        label={'Pronunciation'}
                        content={word.pronunciation}
                        isBusy={isBusy} />
                    <WordDetail
                        label={'Frequency'}
                        content={word.frequency.perMillion}
                        isBusy={isBusy} />
                </div>
                <div className={styles.responsesWrapper}>
                    <WordDetail
                        label={'Definitions'}
                        content={word.definitions.toString()}
                        isBusy={isBusy} />
                </div>
                <div className={styles.responsesWrapper}>
                    <WordDetail
                        label={'Examples'}
                        content={word.examples.toString()}
                        isBusy={isBusy} />
                </div>
                <div className={styles.responsesWrapper}>
                    <WordDetail
                        label={'Synonyms'}
                        content={word.synonyms.toString()}
                        isBusy={isBusy} />
                </div>
            </SwipeableViews> </div>
            : <div className={styles.scrollable}>
                <legend>Origin:</legend>
                <p>{word.origin} </p>
                <WordDetail
                    label={'Translated'}
                    content={word.translated}
                    isBusy={isBusy} />
                <WordDetail
                    label={'Pronunciation'}
                    content={word.pronunciation}
                    isBusy={isBusy} />
                {word.frequency && <WordDetail
                    label={'Frequency'}
                    content={word.frequency.perMillion}
                    isBusy={isBusy} />}
                {word.definitions && <WordDetail
                    label={'Definitions'}
                    content={word.definitions.toString()}
                    isBusy={isBusy} />}
                {word.examples && <WordDetail
                    label={'Examples'}
                    content={word.examples.toString()}
                    isBusy={isBusy} />}
                {word.synonyms && <WordDetail
                    label={'Synonyms'}
                    content={word.synonyms.toString()}
                    isBusy={isBusy} />}
                {word.rhymes && <WordDetail
                    label={'Rhymes'}
                    content={word.rhymes.toString()}
                    isBusy={isBusy} />}
            </div>
    }
    render() {
        const { word, isBusy } = this.props.store
        console.log('details render')
        return <section className={styles.container}>
            <HeaderAction header={'Details'}
                buttonLabel={'Remove'}
                buttonClass={styles.removeButton}
                onClick={this.onRemoveHandler} />
            {word &&
                this.renderDetails(word, isBusy)
            }
        </section>
    }
}

