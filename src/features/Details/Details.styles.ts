import { style } from 'typestyle'
import { rem, percent, important, calc } from 'csx'

import Colors from 'src/common/Colors'


const black5 = 'rgba(15, 15, 0, 0.71)'  // todo move it to colors
const labelSize = 2
const labelMargin = 0.8

const container = style({
    $debugName: 'container',
    height: 'inherit',
    fontFamily: 'Muli, sans-serif',
    $nest: {
        'legend': {
            fontWeight: 600,
            color: black5,
            fontSize: rem(1.5),
            marginBottom: rem(0.8),
        },
        'p': {
            fontWeight: 100,
            margin: rem(0),
            marginBottom: rem(1.2),
            fontSize: rem(1.5),
            color: black5,
        },
    },
})

const removeButton = style({
    $debugName: 'removeButton',
    borderColor: important(Colors.pink.toString()),
    color: important(Colors.pink.toString()),
    $nest: {
        '&:hover': {
            color: important(Colors.white.toString()),
            backgroundColor: important(Colors.violet1.toString()),
        },
    },
})

const scrollable = style({
    $debugName: 'scrollable',
    overflowY: 'auto',
    overflowX: 'hidden',
    height: calc(`${percent(100)} - ${rem(5)}`),
    width: percent(100),
    wordWrap: 'break-word',
})

const responsesWrapper = style({
    $debugName: 'responseWrapper',
    fontFamily: 'Muli, sans-serif',
    $nest: {
        'legend': {
            fontWeight: 600,
            color: black5,
            fontSize: rem(1.5),
            marginBottom: rem(0.8),
        },
        'p': {
            fontWeight: 100,
            margin: rem(0),
            marginBottom: rem(1.2),
            fontSize: rem(1.5),
            color: black5,
        },
    },
})

const swipeableContainer = {
    height: '100%',
    width: '100%',
}

const swipeableWrapper = style({
    $debugName: 'swipeableWrapper',
    height: percent(100),
    $nest: {
        'div:first-child': {
            height: percent(100),
        },
        '.react-swipeable-view-container': {
            height: percent(100),
        },
    },
})

export default {
    container,
    removeButton,
    scrollable,
    responsesWrapper,
    swipeableContainer,
    swipeableWrapper,
}
