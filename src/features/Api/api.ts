import axios, { AxiosInstance, AxiosError } from 'axios'

import {
    GetTranslation, GetDefinition, GetPronunciation, GetFrequency, GetExamples,
    GetRhymes, GetSynonyms, GetList, SaveWordPairs, IApiError, SaveDetails, GetWordDetails, DeleteWord,
} from './model'
import { request } from 'http'

//@ts-ignore
const translateServiceURL = TRANSLATEURL
//@ts-ignore
const apiServiceURL = APIURL

interface IApi {
    getTranslation: GetTranslation
    getDefinition: GetDefinition
    getPronunciation: GetPronunciation
    getFrequency: GetFrequency
    getList: GetList
    getExamples: GetExamples
    getSynonyms: GetSynonyms
    getRhymes: GetRhymes
    getDetails: GetWordDetails
    saveWordsPairs: SaveWordPairs
    saveDetails: SaveDetails
    deleteWord: DeleteWord
}

type ClientProps = {
    handleErrors: (message: string) => void,
    handleSuccess: (message) => void,
}

export type Client = (props: ClientProps) => IApi

export const client: Client = (props) => {
    const client: AxiosInstance = axios.create()
    const onError = (error: AxiosError): IApiError => {
        return (error.response)
            ? { status: error.response.status, message: error.response.request.response }
            : { status: 0, message: 'network or remote service unavailable' }
    }

    client.interceptors.request.use((config) => {
        return config
    }, (error: AxiosError) => {
        props.handleErrors('Network request error')
        return Promise.reject(error)
    })

    client.interceptors.response.use((response) => {
        props.handleSuccess(`Request done: \nStatus:${response.status}`)
        return response
    }, (e) => {
        const error = onError(e)
        props.handleErrors(
            `Network response failure: 
            \nStatus:${error.status} 
            \nDescription: ${error.message}`)
        return Promise.reject(e)
    })

    // todo check is it required to fix types
    const basicDetailRequest = (request, url: string) => {
        return (request) => {
            return client
                .get(`${url}/?word=${request.word}`)
                .then(response =>({...response.data}))
        }
    }

    return {
        getTranslation: (request) => {
            return client
                .get(`${translateServiceURL}/?word=${request.word}`)
                .then(response => response.data)
        }, 
        getList: (id) => {
            return client
                .get(`${apiServiceURL}/wordsPairs/?id=${id}`)
                .then(response => response.data.list)
        },
        saveWordsPairs: (props) => {
            return client
                .post(`${apiServiceURL}/wordsPairs`, props)
                .then(response => null)
        },
        saveDetails: (props) => {
            return client
                .post(`${apiServiceURL}/details`, props)
                .then(response => null)
        },
        getDetails: (word: string) => {
            return client
                .get(`${apiServiceURL}/details/?word=${word}` )
                .then(response => response.data )
        },
        deleteWord: (props) => {
            return client.delete(`${apiServiceURL}/word`, {data: props}).then(response => null)
        },
        getDefinition: basicDetailRequest(request, `${apiServiceURL}/definition`),
        getPronunciation: basicDetailRequest(request, `${apiServiceURL}/pronunciation`),
        getFrequency: basicDetailRequest(request, `${apiServiceURL}/frequency`),
        getExamples: basicDetailRequest(request, `${apiServiceURL}/examples`),
        getSynonyms: basicDetailRequest(request, `${apiServiceURL}/synonyms`),
        getRhymes: basicDetailRequest(request, `${apiServiceURL}/rhymes`),
    }
}





