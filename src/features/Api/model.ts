import { IWordsPairs } from 'src/features/TranslateWord/model'
import { AxiosError } from 'axios'
import { IWordDetail } from '../TranslateWord/WordStore'


export interface ISaveWordsPairsRequest {
    pair: IWordsPairs
    id: string
}

export interface ITranslationRequest {
    word: string
}

export interface ITranslationResponse {
    text: string
    from: {
        language: {
            didYouMean: boolean;
            iso: string; //todo enum
        };
        text: {
            didYouMean: boolean;
            autoCorrected: boolean;
            value: string;
        };
    }
    raw: string
}

export interface IDefinitionRequest {
    word: string
}

export interface IDefinitionResponse {
    definitions: IDefinition[]
}

export interface IDefinition {
    definition: string
    // partOfSpeach: string
}

export interface IPronunciationRequest {
    word: string
}

export interface IPronunciationResponse {
    pronunciation: IPronunciation
}

export interface IPronunciation {
    all: string
}
export interface IFrequencyRequest {
    word: string
}

export interface IFrequencyResponse {
    frequency: IFrequency
}

export interface IFrequency {
    zipf: number
    perMillion: number
    diversity: number
}

export interface IExamplesRequest {
    word: string
}

export interface IExamplesResponse {
    examples: string[]
}

export interface ISynonymsRequest {
    word: string
}

export interface ISynonymsResponse {
    synonyms: string[]
}

export interface IRhymesRequest {
    word: string
}

export interface IRhymesResponse {
    rhymes: IRhymes
}

export interface IRhymes {
    all: string[]
}

export interface IApiError {
    status: number
    message: string
}

export interface IDetails { // todo fields should be optional
    language: string
    origin: string
    pronunciation: string
    translated: string
    frequency: IFrequency
    examples: string[]
    rhymes: string[]
    synonyms: string[]
    definitions: string[]
}

export interface ISaveDetailsRequest {
    word: IDetails
}

export interface IGetDetailsResponses {
    word: IDetails
}

interface IDeleteWord {
    id: string
    word: string
}

export type GetTranslation = (request: ITranslationRequest) => Promise<ITranslationResponse> | Promise<IApiError>
export type GetDefinition = (request: IDefinitionRequest) => Promise<IDefinitionResponse>
export type GetPronunciation = (request: IPronunciationRequest) => Promise<IPronunciationResponse>
export type GetFrequency = (request: IFrequencyRequest) => Promise<IFrequencyResponse>
export type GetExamples = (request: IExamplesRequest) => Promise<IExamplesResponse>
export type GetSynonyms = (request: ISynonymsRequest) => Promise<ISynonymsResponse>
export type GetRhymes = (request: IRhymesRequest) => Promise<IRhymesResponse>
export type GetList = (id: string) => Promise<IWordsPairs[]>
export type GetWordDetails = (word: string) => Promise<IDetails>
export type SaveDetails = (props: ISaveDetailsRequest) => Promise<null>
export type SaveWordPairs = (props: ISaveWordsPairsRequest) => Promise<null>
export type DeleteWord = (props: IDeleteWord) => Promise<null>

