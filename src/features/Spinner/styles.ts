import { style, keyframes } from 'typestyle'

import colors from 'src/common/Colors'

const defaultSize = 40

const skFoldCubeAngle = keyframes({
    '0%, 10%': {
        '-webkit-transform': 'perspective(140px) rotateX(-180deg)',
        transform: 'perspective(140px) rotateX(-180deg)',
        opacity: 0,
    }, '25%, 75%': {
        '-webkit-transform': 'perspective(140px) rotateX(0deg)',
        transform: 'perspective(140px) rotateX(0deg)',
        opacity: 1,
    }, '90%, 100%': {
        '-webkit-transform': 'perspective(140px) rotateY(180deg)',
        transform: 'perspective(140px) rotateY(180deg)',
        opacity: 0,
    },
})

const skFoldingCube = style({
    margin: '20px auto',
    width: '40px',
    height: '40px',
    position: 'relative',
    '-webkit-transform': 'rotateZ(45deg)',
    transform: 'rotateZ(45deg)',
    $nest: {
        '& .skCube': {
            float: 'left',
            width: '50%',
            height: '50%',
            position: 'relative',
            '-webkit-transform': 'scale(1.1)',
            '-ms-transform': 'scale(1.1)',
            transform: 'scale(1.1)',
        },
        '& .skCube:before': {
            content: '""',
            position: 'absolute',
            top: '0',
            left: '0',
            width: '100%',
            height: '100%',
            backgroundColor: colors.pink.toString(),
            '-webkit-animation': 'sk-foldCubeAngle 2.4s infinite linear both',
            animation: `${skFoldCubeAngle} 2.4s infinite linear both`,
            '-webkit-transform-origin': '100% 100%',
            '-ms-transform-origin': '100% 100%',
            'transform-origin': '100% 100%',
        },
        '& .skCube2': {
            '-webkit-transform': 'scale(1.1) rotateZ(90deg)',
            transform: 'scale(1.1) rotateZ(90deg)',
        },
        '& .skCube3': {
            '-webkit-transform': 'scale(1.1) rotateZ(180deg)',
            transform: 'scale(1.1) rotateZ(180deg)',
        },
        '& .skCube4': {
            '-webkit-transform': 'scale(1.1) rotateZ(270deg)',
            transform: 'scale(1.1) rotateZ(270deg)',
        },
        '& .skCube2:before': {
            '-webkit-animation-delay': '0.3s',
            animationDelay: '0.3s',
        },
        '& .skCube3:before': {
            '-webkit-animation-delay': '0.6s',
            animationDelay: '0.6s',
        },
        '& .skCube4:before': {
            '-webkit-animation-delay': '0.9s',
            animationDelay: '0.9s',
        },
    },
})

export default {skFoldingCube, defaultSize}
