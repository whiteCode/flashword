

import * as React from 'react'

import styles from './styles'
import colors from 'src/common/Colors'

interface ISpinnerProps {
    size?: number
}


export default class Spinner extends React.PureComponent<ISpinnerProps> {
    render() {
        const { size } = this.props
        return (
            <div
                className={styles.skFoldingCube}
                style={{ height: size ? size : styles.defaultSize, width: size ? size : styles.defaultSize}}>
                <div className="skCube1 skCube" />
                <div className="skCube2 skCube" />
                <div className="skCube4 skCube" />
                <div className="skCube3 skCube" />
            </div>
        )
    }
}
