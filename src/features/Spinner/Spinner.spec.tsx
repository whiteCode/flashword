import 'jest'
import { shallow } from 'enzyme'
import * as React from 'react'

import Spinner from './Spinner'

it('renders correctly', () => {
    const component = shallow(<Spinner/>)
    expect(component).toMatchSnapshot()
})

it('renders correctly with custom size', () => {
    const component = shallow(<Spinner size={120}/>)
    expect(component).toMatchSnapshot()
})
