import * as React from 'react'
import { action } from 'mobx'
import FacebookLogin from 'react-facebook-login'

import { IUserProfile } from 'src/features/UserProfile/UserProfileStore'
import styles from './styles'

interface ILoginProps {
    onSetProfile: (props: IUserProfile) => void
}

interface IAuthorizationResponse {
    name: string
    id: string
    picture: {
        data: {
            url: string,
        },
    }
}

export default class Login extends React.PureComponent<ILoginProps> {
    @action.bound
    onFacebookLogin(response: IAuthorizationResponse) {
        const { name, id } = response
        this.props.onSetProfile({
            name,
            id,
            picture: response.picture.data.url,
        })
    }
    render() {
        return (
            <FacebookLogin
                appId="961678917320720"
                autoLoad={true}
                fields="name,email,picture"
                callback={this.onFacebookLogin}
                cssClass={styles.navLink}
                icon="fa-facebook"
                disableMobileRedirect={true}
            />
        )
    }
}
