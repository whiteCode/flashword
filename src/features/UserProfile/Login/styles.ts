import { style } from 'typestyle'
import { rem, percent } from 'csx'

import colors from 'src/common/Colors'

const fullWidth = 100
const fontSize = 1.2

const navLink = style({
    $debugName: 'navLink',
    backgroundColor: colors.violet.toString(),
    color: colors.blue.toString(),
    border: 'none',
    width: percent(fullWidth),
    padding: rem(1),
    textAlign: 'left',
    fontSize: rem(1),
    $nest: {
        '&:focus': {
            outline: 'none',
        },
        '&:hover': {
            backgroundColor: colors.violet1.toString(),
        },
        i: {
            padding: `${rem(0)} ${rem(1)}`,
            fontSize: rem(fontSize),
            color: '#f0f0f067',
        },
    },
})

export default { navLink }
