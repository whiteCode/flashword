import 'jest'
import { shallow } from 'enzyme'
import * as React from 'react'

import Login from './Login'

it('renders correctly', () => {
    const component = shallow(<Login onSetProfile={() => {}} />)
    expect(component).toMatchSnapshot()
})
