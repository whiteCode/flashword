import 'jest'
import { autorun } from 'mobx'

import UserProfileStore from './UserProfileStore'

describe('UserProfile Store Public Api', () => {
    let store: UserProfileStore

    beforeEach(() => {
        store = new UserProfileStore()
    })

    it('does return undefined value before user profile set', () => {
        expect(store.id).toBeUndefined()
    })
    
    it('does return correct user profile id after set', () => {
        const testId = 'id123'
        store.onSetProfile({id: testId, name: 'dummy', picture: '2'})
        expect(store.id).toEqual(testId)
    })

    it('does return correct profile name', () => {
        const testName = 'name'
        store.onSetProfile({id: '', name: testName, picture: '2'})
        expect(store.profile.name).toEqual(testName)
    })

    it('does return correct profile picture', () => {
        const testPicture = 'src/dummy'
        store.onSetProfile({id: '', name: '', picture: testPicture})
        expect(store.profile.picture).toEqual(testPicture)
    })

    it('does notify subscribers about change in profile', () => {
        const spySubscriber = jest.fn()
        const dispose = autorun(() => {
            const trigger = store.profile
            spySubscriber()
        })
        store.onSetProfile({id: '', name: '', picture: ''})
        process.nextTick(() => expect(spySubscriber.mock.calls.length).toEqual(2))
        dispose()
    })  
})
