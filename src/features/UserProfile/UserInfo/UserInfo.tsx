import * as React from 'react'
import { observer } from 'mobx-react'

import styles from './styles'

interface IUserInfoProps {
    picture: string
    name: string
}

@observer
export default class UserInfo extends React.Component<IUserInfoProps> {
    render() {
        const { picture, name } = this.props
        return (
            <header className={styles.header}>
                <img src={picture} className={styles.picture} />
                <span> {name} </span>
            </header>
        )
    }
}
