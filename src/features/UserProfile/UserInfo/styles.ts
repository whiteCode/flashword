import { style } from 'typestyle'
import { rem, px, percent } from 'csx'

const pictureHeight = rem(3)

const header = style({
    $debugName: 'header',
    height: pictureHeight,
    padding: `${rem(0.5)} ${rem(0.95)}`,
    display: 'flex',
    marginBottom: rem(1.5),
    $nest: {
        span: {
            padding: `${rem(0)} ${rem(1)}`,
            lineHeight: pictureHeight,
            verticalAlign: 'middle',
        },
    },
})

const picture = style({
    $debugName: 'picture',
    borderRadius: percent(50),
    height: pictureHeight,
    width: pictureHeight,
})

export default { header, picture }
