import 'jest'
import { shallow } from 'enzyme'
import * as React from 'react'

import UserInfo from './UserInfo'

it('renders correctly', () => {
    const component = shallow(<UserInfo picture="https://dummyurl/" name="Dummy Name" />)
    expect(component).toMatchSnapshot()
})
