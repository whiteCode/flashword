import { observable } from 'mobx'
import { action } from 'mobx'

export interface IUserProfile {
    id: string    
    name: string
    picture: string
}

export default class UserProfileStore {
    @observable profile: IUserProfile 
    get id(): string | undefined {
        return this.profile && this.profile.id
    }

    @action.bound
    onSetProfile({ id, name, picture }: IUserProfile) {
        this.profile = {
            id,            
            name,
            picture,
        }
    }
}
