import { style } from 'typestyle'
import { rem, percent, border, px, important } from 'csx'

import colors from 'src/common/Colors'


const black5 = 'rgba(15, 15, 0, 0.71)'
const black10 = 'rgba(15, 15, 0, 0.2)'

const labelSize = 2
const labelMargin = 0.8

const listWrapper = style({
    height: percent(100),
    clear: 'both',
})

const container = style({
    $debugName: 'container',
    height: 'inherit',
    fontFamily: 'Muli, sans-serif',
    $nest: {
        'li': {
            textTransform: 'capitalize',
            width: rem(20),
            $nest: {
                'div': {
                    fontWeight: 100,
                    width: percent(50),
                    color: black5,
                    fontSize: rem(1.5),
                    paddingBottom: rem(0.6),
                    float: 'left',
                },

            },
        },
        'section': {
            height: `calc(${percent(100)} - ${rem(labelMargin + labelSize)})`,
            overflowY: 'auto',
            overflowX: 'hidden',
        },
        'label': {
            fontWeight: 600,
            color: black5,
            fontSize: rem(1.5),
            marginBottom: rem(0.8),         
        },
    },
})

const wrapper = style({
    $debugName: 'wrapper',
    height: percent(100),
    $nest: {
        'div:first-child': {
            height: percent(100),
        },
        '.react-swipeable-view-container': {
            height: percent(100),
        },
    },
})

const columnsWrapper = style({
    $debugName: 'columnsWrapper',
    height: percent(100),
    width: percent(100),
    display: 'flex',
    $nest: {
        'header': {
            fontSize: rem(3),
            fontWeight: 400,
            lineHeight: rem(3.2),
            wordSpacing: rem(0.6),
            fontFamily: 'Muli, sans-serif',
            opacity: 0.8,
            marginBottom: rem(2),
        },
        'ul': {
            height: `calc(${percent(100)} - ${rem(6.7)})`,
            overflow: 'auto',
        },
        'li': {
            textTransform: 'capitalize',
            width: rem(26),
            $nest: {
                'div': {
                    fontWeight: 100,
                    width: percent(50),
                    color: black5,
                    fontSize: rem(1.5),
                    paddingBottom: rem(0.6),
                    float: 'left',
                    height: important(`${rem(2.1)}`),
                },
                '&:hover': {
                    cursor: 'pointer',
                },

            },
        },
        'label': {
            fontWeight: 600,
            color: black5,
            fontSize: rem(1.5),
            marginBottom: rem(0.8),         
        },
    },
})

const column = style({
    $debugName: 'column',
    height: percent(100),
    width: percent(50),
})

const secondColumn = style({
    $debugName: 'secondColumn',
    borderLeft: border({ style: 'solid', width: px(1), color: black10 }),
    paddingLeft: rem(2),
})




export default {
    container,
    wrapper,
    columnsWrapper,
    column,
    secondColumn,
    listWrapper,
}
