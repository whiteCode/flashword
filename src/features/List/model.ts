import { GetList, GetWordDetails, DeleteWord } from 'src/features/Api/model'

export interface IListApi {
    getList: GetList
    getDetails: GetWordDetails
    deleteWord: DeleteWord
}

export enum VisibleListSection {
    List = 0,
    Filters = 1,
    Details = 2,
}
