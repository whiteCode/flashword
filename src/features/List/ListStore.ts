import { observable, computed, action } from 'mobx'
import { promisedComputed } from 'computed-async-mobx'
import bind from 'bind-decorator'

import DetailsStore from 'src/features/Details/DetailsStore'
import { IWordsPairs } from 'src/features/TranslateWord/model'
import { IListApi, VisibleListSection } from './model'
import { ICommon } from 'src/features/App/model'

interface IListStoreProps {
    api: IListApi,
    common: ICommon
}

export default class ListStore {
    @observable requestCounter = 0

    details: DetailsStore = new DetailsStore({
        common: this.props.common,
        api: {
            getDetails: this.props.api.getDetails,
            deleteWord: this.props.api.deleteWord,
        },
        onDeleteWordSuccess: this.onDeleteWordSuccess,
    })

    @observable visibleSection: VisibleListSection = VisibleListSection.List

    @computed get list(): IWordsPairs[] {
        return this.requestList.get()
    }

    constructor(private props: IListStoreProps) { }

    @action.bound
    onChangeSection(index: VisibleListSection) {
        this.visibleSection = index
    }

    @action.bound
    onShowDetails(word: string) {
        this.onChangeSection(VisibleListSection.Details)
        this.details.origin = word
    }

    @bind
    onDeleteWordSuccess() {
        this.fetchData()     
        this.onChangeSection(VisibleListSection.Filters)
    }

    @bind
    fetchData() {
        this.requestCounter++
    }

    listInitialValue = []

    private requestList = promisedComputed(this.listInitialValue, async () => {
        const id = this.props.common.getUserID()
        const trigger = this.requestCounter
        return (id)
            ? this.props.api.getList(id)
                .catch(error => this.listInitialValue)
            : this.listInitialValue
    })
}
