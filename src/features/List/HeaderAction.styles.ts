import { style } from 'typestyle'
import { rem, percent, border, px, important } from 'csx'

import Colors from 'src/common/Colors'

const actionLine = style({
    $debugName: 'actionLine',
    height: important(`${rem(5)}`),
    $nest: {
        'header': {
            float: 'left',
            textTransform: 'capitalize',            
            fontSize: rem(3),
            fontWeight: 400,
            lineHeight: rem(3.2),
            wordSpacing: rem(0.6),
            fontFamily: 'Muli, sans-serif',
            opacity: 0.8,
        },   
        'button': {
            color: Colors.green.toString(),
            backgroundColor: Colors.white.toString(),
            float: 'right',
            borderRadius: rem(0.2),
            fontSize: rem(1.3),
            padding: `${rem(0.1)} ${rem(1)}`,
            outline: 0,
            letterSpacing: px(1),
            fontWeight: 600,
            fontFamily: 'Muli, sans-serif',
            textTransform: 'uppercase',
            border: border({ style: 'solid', width: px(2), color: Colors.green.toString() }),
            $nest: {
                '&:hover': {
                    color: Colors.white.toString(),
                    backgroundColor: Colors.violet1.toString(),
                    border: border({ style: 'solid', width: px(2), color: Colors.violet.toString() }),

                    cursor: 'pointer',
                },
            },
            transition: '0.2s',
        },
    },
})

export default { actionLine } 
