import * as React from 'react'
import { action } from 'mobx'
import { observer } from 'mobx-react'
import { classes } from 'typestyle'

import { IWordsPairs } from 'src/features/TranslateWord/model'
import styles from './styles'
import DetailsStore from 'src/features/Details/DetailsStore'
import { VisibleListSection } from './model'
import HeaderAction from './HeaderAction'
import { Device } from '../DeviceSpecificLayout'

export interface IListProps {
    list: IWordsPairs[]
    details: DetailsStore
    visibleSection: VisibleListSection
    onChangeSection: (index: VisibleListSection) => void
    onShowDetails: (word: string) => void
    deviceType: Device
}


@observer
export class List extends React.Component<IListProps> {
    @action.bound
    onShowDetails(word: string) {
        this.props.onShowDetails(word)
    }

    @action.bound
    onShowFilters() {
        this.props.onChangeSection(VisibleListSection.Filters)
    }


    render() {
        const { list, details, visibleSection, deviceType } = this.props

        return (
            <section className={styles.container}>
                <HeaderAction 
                    header={'List'} 
                    buttonLabel={visibleSection === VisibleListSection.Details || deviceType === Device.Mobile ? 'Show Filters' : undefined } 
                    onClick={this.onShowFilters} 
                />
                <div className={styles.listWrapper}>
                    <ul>
                        {list.map((pair: IWordsPairs, index: number) => {
                            return (
                                <li key={`pair` + index} onClick={() => this.onShowDetails(pair.originWord)}>
                                    <div>
                                        {pair.originWord}
                                    </div>
                                    <div>
                                        {pair.translatedWord}
                                    </div>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </section>
        )
    }
}
