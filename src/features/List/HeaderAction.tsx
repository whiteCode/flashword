import * as React from 'react'

import styles from './HeaderAction.styles'

interface IHeaderActionsProps {
    header: string
    buttonLabel?: string
    buttonClass?: string
    
    onClick: () => void
}

export default class HeaderActions extends React.Component<IHeaderActionsProps> {
    render() {
        const { header, buttonLabel, buttonClass } = this.props

        return (
            <div className={styles.actionLine}>
                <header> {header} </header>
                {buttonLabel && <button className={buttonClass}
                    onClick={this.props.onClick}> {buttonLabel} </button>}
            </div>)
    }
}
