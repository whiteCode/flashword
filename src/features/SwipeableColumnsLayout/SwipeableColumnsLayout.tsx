import * as React from 'react'
import { action } from 'mobx'
import { observer } from 'mobx-react'
import SwipeableViews from 'react-swipeable-views'
import { classes } from 'typestyle'

import styles from './SwipeableColumnsLayout.styles'

@observer
export class Mobile extends React.Component<{visibleSection, onSectionChange}> {
    render() {
        return (
            <div className={styles.swipeableWrapper}>
                <SwipeableViews
                    index={this.props.visibleSection }
                    onChangeIndex={this.props.onSectionChange}>
                    {this.props.children}
                </SwipeableViews>
            </div>
        )
    }
}

@observer
export class Desktop extends React.Component {
    render() {
        return (
            <div className={styles.columnsWrapper}>
                <div className={styles.column}>
                    {this.props.children && this.props.children[0]}
                </div>
                <div className={classes(styles.column, styles.secondColumn)}>
                    {this.props.children && this.props.children[1]}
                </div>
            </div>
        )
    }
}

