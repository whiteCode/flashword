import * as React from 'react'

import TextPage from 'src/features/TextPage/TextPage'
export default class EmptyPage extends React.PureComponent {
    render() {
        return <TextPage>
            <div>
                <header> Empty Page </header>
            </div >
        </TextPage>
    }
}

