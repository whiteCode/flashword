import 'jest'
import { shallow } from 'enzyme'
import * as React from 'react'

import { MobileLayout, DesktopLayout } from './Layout'

it('renders correctly - mobile', () => {
    const component = shallow(
        <MobileLayout
            visibleSection={0}
            navigation={<div> navigation </div>}
            header={<div> header </div>}
            footer={<div> footer </div>}
            onSectionChanged={() => { }}
        >
            {' '}
            <div> Content </div>{' '}
        </MobileLayout>,
    )
    expect(component).toMatchSnapshot()
})

it('renders correctly - desktop', () => {
    const component = shallow(
        <DesktopLayout
            visibleSection={1}
            navigation={<div> navigation </div>}
            header={<div> header </div>}
            footer={<div> footer </div>}
            onSectionChanged={() => { }}
        >
            {' '}
            <div> Content </div>{' '}
        </DesktopLayout>,
    )
    expect(component).toMatchSnapshot()
})

// todo add cases for sections
