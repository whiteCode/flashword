import { style } from 'typestyle'
import { percent, rem, border, px } from 'csx'
import colors from 'src/common/Colors'

const fullHeight = 100
const basis = 5
const navFlexBasis = 17

const layout = style({
    $debugName: 'layout',
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: percent(fullHeight),
    $nest: {
        '& .react-swipeable-view-container': {
            height: percent(100),
        },
    },
})

const layoutDesktop = style({
    $debugName: 'layoutDesktop',
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    height: percent(fullHeight),
})

const nav = style({
    $debugName: 'nav',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    flexBasis: rem(navFlexBasis),
    backgroundColor: colors.violet.toString(),
    color: colors.white.toString(),
    fontSize: rem(1),
    height: percent(100),
    userSelect: 'none',
    '-webkit-touch-callout': 'none',
})

const linksContainer = style({
    $debugName: 'linkContainer',
    flexGrow: 1,
})

const List = style({
    $debugName: 'List',
    flexBasis: rem(basis),
    borderLeft: border({ style: 'solid', width: px(1) }),
    borderColor: '#000000',
    flex: 1,
})

const swipeableContainer = {
    height: '100%',
    width: '100%',
}


export default {
    layout,
    layoutDesktop,
    nav,
    linksContainer,
    List,
    swipeableContainer,
}
