import * as React from 'react'
import SwipeableViews from 'react-swipeable-views'

import styles from './styles'
import { VisibleSection } from 'src/features/Layout/model'

export interface ILayoutProps {
    navigation: JSX.Element
    header: JSX.Element
    footer: JSX.Element
    visibleSection: VisibleSection
    onSectionChanged: (index: number) => void
}

export class DesktopLayout extends React.Component<ILayoutProps> {

    render(): JSX.Element {
        
        return <section className={styles.layoutDesktop}>
            <nav className={styles.nav}>
                {this.props.header}
                <div className={styles.linksContainer}>{this.props.navigation}</div>
                {this.props.footer}
            </nav>
            {this.props.children}
        </section>
    }
}

export class MobileLayout extends React.Component<ILayoutProps, {}> {
    render(): JSX.Element {        
        return <section className={styles.layout}>
            <SwipeableViews 
                style={styles.swipeableContainer}
                index={this.props.visibleSection}
                onChangeIndex={this.props.onSectionChanged}
                >
                <nav className={styles.nav}>
                    {this.props.header}
                    <div className={styles.linksContainer}>{this.props.navigation}</div>
                    {this.props.footer}
                </nav>
                {this.props.children}
            </SwipeableViews>
        </section >
    }
}
