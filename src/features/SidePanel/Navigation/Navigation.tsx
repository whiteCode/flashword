import * as React from 'react'
import { NavLink } from 'react-router-dom'
import {action} from 'mobx'

import { EntypoDocuments, EntypoEdit, EntypoExport, EntypoHelp, EntypoOpenBook } from 'react-entypo'
import style from './styles'
import { VisibleSection } from 'src/features/Layout/model'

interface INavigationProps {
    login: JSX.Element,
    onRouteChanged: (index: number) => void
}

interface INavigationItem {
    className: string
    activeClassName: string
    to: string
    icon: JSX.Element
    label: string
}

export default class Navigation extends React.Component<INavigationProps> {
    
    //move it to store
    navigationItems = [
        {
            className: style.navLink,
            activeClassName: style.navLinkActive,
            to: '/about',
            icon: <EntypoDocuments />,
            label: 'About',
        }, {
            className: style.navLink,
            activeClassName: style.navLinkActive,
            to: '/translate',
            icon: <EntypoEdit />
            ,
            label: 'Translation',
        }, {
            className: style.navLink,
            activeClassName: style.navLinkActive,
            to: '/list',
            icon: <EntypoOpenBook />,
            label: 'List',
        }, {
            className: style.navLink,
            activeClassName: style.navLinkActive,
            to: '/tests',
            icon: <EntypoHelp />,
            label: 'Tests',
        }, {
            className: style.navLink,
            activeClassName: style.navLinkActive,
            to: '/exports',
            icon: <EntypoExport />,
            label: 'Exports',
        },
    ]

    @action.bound
    itemSelectHandler() {
        this.props.onRouteChanged(VisibleSection.ContentPanel)
    }

    render() {
        return (
            <div>
                {this.navigationItems.map((item, index) => {
                    return <div onClick={this.itemSelectHandler} key={'nav' + index}>
                        <NavLink
                            className={item.className}
                            activeClassName={item.activeClassName}
                            to={item.to}
                        >
                            {item.icon}
                            {item.label}
                        </NavLink>
                    </div>
                })}
                {this.props.login}
            </div>
        )
    }
}
