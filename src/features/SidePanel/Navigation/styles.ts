import { style } from 'typestyle'
import { rem } from 'csx'

import colors from 'src/common/Colors'

const navLink = style({
    $debugName: 'navLink',
    fontWeight: 400,
    display: 'block',
    padding: rem(1),
    color: colors.white.toString(),
    textDecoration: 'none',
    fontFamily: 'Lato, sans-serif',    
    $nest: {
        '&:hover': {
            backgroundColor: colors.violet1.toString(),
        },
        svg: {
            padding: `${rem(0)} ${rem(1)}`,
            fontSize: rem(1),
            color: '#f0f0f067',
        },
    },
})

const navLinkActive = style({
    backgroundColor: colors.green.toString(),
    $nest: {
        svg: {
            color: '#f0f0f067',
        },
    },
})

export default { navLink, navLinkActive }
