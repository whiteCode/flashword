import * as React from 'react'

import styles from './styles'

export const Footer = () => {
    return (
        <footer className={styles.footer}>
            <span className={styles.companyName}> FlashWord </span> Made by AIT
        </footer>
    )
}
