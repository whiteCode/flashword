import { style } from 'typestyle'
import { rem, px } from 'csx'

import colors from 'src/common/Colors'

const footer = style({
    $debugName: 'footer',
    height: rem(1),
    textAlign: 'right',
    alignSelf: 'flex-end',
    paddingBottom: px(6),
    paddingRight: px(6),
})

const companyName = style({
    $debugName: 'companyName',
    color: colors.yellow.toString(),
    fontWeight: 900,
})

export default {
    footer,
    companyName,
}
