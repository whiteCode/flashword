import 'jest'
import { autorun } from 'mobx'

import WordPairsStore from './WordsPairs'
import { ApiWordsPairsMock, getUserID, wordsPairs, newPair } from './factory'

describe('WordPairs Store Public Api', () => {
    let store: WordPairsStore
    let disposeSubscribers

    beforeEach(() => {
        store = new WordPairsStore({ api: ApiWordsPairsMock, getUserID: getUserID, onSaveNewPair: () => null })
        disposeSubscribers = autorun(() => {
            //@ts-ignore
            const trigger = store.processedList
        })
    })

    // afterEach(() => {
    //     disposeSubscribers()
    // })

    it('does set new pair after pair create', () => {
        const spy = jest.fn()
        store.onNewPairCreated(newPair)
        expect(store.currentPair).toEqual(newPair)
    })

    // it('does call api save method on add new pair to list', () => {
    //     const spy = jest.fn().
    //     store = new WordPairsStore({ api: { ...ApiWordsPairsMock, saveWordsPairs: spy }, getUserID: getUserID })
    //     disposeSubscribers = autorun(() => {
    //         const trigger = store.processedList
    //     })
    //     store.onNewPairCreated(newPair)
    //     expect(spy.mock.calls.length).toEqual(1)
    // })

    // it('does not call api save method on add new pair to list when user id is not defined ', () => {
    //     const spy = jest.fn()
    //     store = new WordPairsStore({ api: { ...ApiWordsPairsMock, saveWordsPairs: spy }, getUserID: () => undefined })
    //     disposeSubscribers = autorun(() => {
    //         const trigger = store.processedList
    //     })
    //     store.onNewPairCreated(newPair)
    //     expect(spy.mock.calls.length).toEqual(0)
    // })

    // it('does fetch data on load', () => {
    //     process.nextTick(() => {
    //         expect(store.processedList).toEqual(wordsPairs)
    //     })
    // })
})

