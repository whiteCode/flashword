import * as React from 'react'
import { observer } from 'mobx-react'
import SwipeableViews from 'react-swipeable-views'
import { virtualize } from 'react-swipeable-views-utils'

import TextBox from './TextBox/TextBox'
import { IWordsPairs } from './model'
import styles from './styles'
import Spinner from 'src/features/Spinner/Spinner'
import { IFrequency } from 'src/features/Api/model'
import WordDetail from 'src/features/TranslateWord/WordDetail'
import WordStore, { IWordDetail } from 'src/features/TranslateWord/WordStore'
import { Device } from 'src/features/DeviceSpecificLayout'


interface ITranslateWord {
    word?: WordStore
    deviceType: Device
    onSubmit: (value: string) => void
}

const VirtualizeSwipeableViews = virtualize(SwipeableViews)

@observer
class DetailsPage1 extends React.Component<{ key: string, word: WordStore }> // todo move it
{
    render() {
        const { key, word } = this.props
        return <div className={styles.responsesWrapper} key={key}>
            <legend>Origin:</legend>
            <p>{word.origin} </p>
            <WordDetail
                label={word.translated.section}
                content={word.translated.value.text}
                isBusy={word.translated.isBusy} />
            <WordDetail
                label={word.pronunciation.section}
                content={word.pronunciation.value.pronunciation.all}
                isBusy={word.pronunciation.isBusy} />
            <WordDetail
                label={word.frequency.section}
                content={word.frequency.value.frequency.perMillion}
                isBusy={word.frequency.isBusy} />
        </div>
    }
}

@observer
class DetailsPage2 extends React.Component<{ key: string, word: WordStore }>  // todo move it
{
    render() {
        const { key, word } = this.props
        return <div className={styles.responsesWrapper} key={key}>
            <WordDetail
                label={word.definitions.section}
                content={word.definitions.value.definitions.toString()}
                isBusy={word.definitions.isBusy} />
        </div>
    }
}

@observer
class DetailsPage3 extends React.Component<{ key: string, word: WordStore }>  // todo move it
{
    render() {
        const { key, word } = this.props
        return <div className={styles.responsesWrapper} key={key}>
            <WordDetail
                label={word.examples.section}
                content={word.examples.value.examples.toString()}
                isBusy={word.examples.isBusy} />
        </div>
    }
}


@observer
class DetailsPage4 extends React.Component<{ key: string, word: WordStore }>  // todo move it
{
    render() {
        const { key, word } = this.props
        return <div className={styles.responsesWrapper} key={key}>
            <WordDetail
                label={word.synonyms.section}
                content={word.synonyms.value.synonyms.toString()}
                isBusy={word.synonyms.isBusy} />
        </div>
    }
}

@observer
class DetailsPage5 extends React.Component<{ key: string, word: WordStore }>  // todo move it
{
    render() {
        const { key, word } = this.props
        return <div className={styles.responsesWrapper} key={key}>
            <WordDetail
                label={word.rhymes.section}
                content={word.rhymes.value.rhymes.all.toString()}
                isBusy={word.rhymes.isBusy} />
        </div>
    }
}

@observer
export default class TranslateWord extends React.Component<ITranslateWord> {
    slideRenderer({ key, index }, props) {
        const { word } = props
        return [
            <DetailsPage1
                key={key}
                word={word} />,
            <DetailsPage2
                key={key}
                word={word} />,
            <DetailsPage3
                key={key}
                word={word} />,
            <DetailsPage4
                key={key}
                word={word} />,
            <DetailsPage5
                key={key}
                word={word} />][Math.abs(index) % 5]
    }

    renderDetails(word: WordStore) {
        const { deviceType } = this.props
        return deviceType === Device.Mobile
            ? <VirtualizeSwipeableViews
                style={styles.swipeableContainer}
                overscanSlideAfter={1}
                overscanSlideBefore={1}
                slideRenderer={({ key, index }) => this.slideRenderer({ key, index }, this.props)} />
            : <>
                <DetailsPage1
                    key={'1'}
                    word={word} />
                <DetailsPage2
                    key={'2'}
                    word={word} />
                <DetailsPage3
                    key={'3'}
                    word={word} />
                <DetailsPage4
                    key={'4'}
                    word={word} />
            </>
    }

    render() {
        const { onSubmit, word } = this.props

        return (
            <section className={styles.container}>
                {word && word.origin
                    ? this.renderDetails(word)
                    : <div className={styles.textWrapper}>
                        <div className={styles.textContainer}>
                            <header> Type your word </header>
                            <p>And translate to your native language.
                            Just simple like that. </p>
                        </div>
                    </div>}
                <div>
                    <TextBox onSubmit={onSubmit} />
                </div>
            </section>
        )
    }
}





