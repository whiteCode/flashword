import React, { Fragment } from 'react'

import Spinner from 'src/features/Spinner/Spinner'
import { observer } from 'mobx-react'

interface IWordDetailProps {
    label: string
    content: string | number
    isBusy: boolean
}

@observer
export default class WordDetail extends React.Component<IWordDetailProps> {

    render() {
        const { label, content, isBusy } = this.props
        return <>
            <legend>{label}: </legend>
            {isBusy
                ? <Spinner size={80} />
                : <p>{content}</p>}
        </>
    }
}

