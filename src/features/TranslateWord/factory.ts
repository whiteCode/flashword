import { IWordsPairs, IWordsPairsApi, ITranslateWordApi } from './model'
import {
    ITranslationResponse, IDefinitionResponse, IDefinition, IFrequencyResponse,
    IFrequency, IExamplesResponse, IPronunciation, IPronunciationResponse,
    IRhymesResponse, ISynonymsResponse, GetList, GetDefinition,
    GetExamples, GetFrequency, GetPronunciation, GetRhymes, GetSynonyms,
    GetTranslation, SaveWordPairs, SaveDetails,
} from 'src/features/Api/model'
import { IListProps } from 'src/features/List/List'
import { client } from 'src/features/Api/api'

export const wordsPairs: IWordsPairs[] = [
    {
        originWord: 'dummy origin',
        translatedWord: 'dummy translate',
    }, {
        originWord: 'dummy origin 2',
        translatedWord: 'dummy translate 2',
    },
]

export const newPair: IWordsPairs = { originWord: 'word', translatedWord: 'translatedWord' }

export const getUserID = (): string => {
    return 'userId'
}

export const getWordsPairsMock: GetList = (id) => new Promise<IWordsPairs[]>((resolve) => {
    resolve(wordsPairs)
})

export const saveWordsPairsMock: SaveWordPairs = (props) => new Promise<null>((resolve) => {
    resolve(null)
})

export const saveDetailsMock: SaveDetails = (props) => new Promise<null>((resolve) => {
    resolve(null)
})


export const translationResponse: ITranslationResponse = {
    text: 'text',
    from: {
        language: {
            didYouMean: false,
            iso: 'eng',
        },
        text: {
            didYouMean: false,
            autoCorrected: false,
            value: 'text',
        },
    },
    raw: 'raw',
}

export const definitionResponse: IDefinitionResponse = {
    definitions: [{
        definition: 'dummy definition',
    }],
}

export const getTranslationMock: GetTranslation = (request) => new Promise<ITranslationResponse>((resolve) => {
    resolve(translationResponse)
})

export const getDefinitionMock: GetDefinition = (request) => new Promise<IDefinitionResponse>((resolve) => {
    resolve(definitionResponse)
})

export const frequency: IFrequency = {
    zipf: 1,
    diversity: 2,
    perMillion: 3,
}


export const getFrequencyMock: GetFrequency = (request) => new Promise<IFrequencyResponse>((resolve) => {
    resolve({ frequency: frequency })
})

export const getExamplesMock: GetExamples = (request) => new Promise<IExamplesResponse>((resolve) => {
    resolve({ examples: ['dummy ex'] })
})

export const getPronunciationMock: GetPronunciation = (request) => new Promise<IPronunciationResponse>((resolve) => {
    resolve({ pronunciation: { all: 'dummy pronunciation' } })
})

export const getSynonymsMock: GetSynonyms = (request) => new Promise<ISynonymsResponse>((resolve) => {
    resolve({ synonyms: ['dummy'] })
})

export const getRhymesMock: GetRhymes = (request) => new Promise<IRhymesResponse>((resolve) => {
    resolve({
        rhymes: {
            all: ['dummy'],
        },
    })
})

export const ApiWordsPairsMock: IWordsPairsApi = {
    saveWordsPairs: saveWordsPairsMock,
}

//todo fix it
export const ApiTranslateWordMock: ITranslateWordApi = {
    saveWordsPairs: saveWordsPairsMock,
    getTranslation: getTranslationMock,
    getDefinition: getDefinitionMock,
    getFrequency: getFrequencyMock,
    getExamples: getExamplesMock,
    getPronunciation: getPronunciationMock,
    getSynonyms: getSynonymsMock,
    getRhymes: getRhymesMock,
    saveDetails: saveDetailsMock,
}

