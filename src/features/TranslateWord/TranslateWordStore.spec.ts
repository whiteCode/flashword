import 'jest'
import { autorun } from 'mobx'

import TranslateWordStore from './TranslateWordStore'
import { ApiTranslateWordMock, getUserID, translationResponse, wordsPairs } from './factory'

describe('TranslateWordStore Public Api', () => {
    let store: TranslateWordStore

    beforeEach(() => {
        store = new TranslateWordStore({ api: ApiTranslateWordMock, common: { getUserID: getUserID }, onSaveNewPair: () => null })
    })

    it('does make api request for translated word when new word is submit', () => {
        const spy = jest.fn()
        spy.mockReturnValue(translationResponse)
        store = new TranslateWordStore({ api: { ...ApiTranslateWordMock, getTranslation: spy }, common: { getUserID: getUserID }, onSaveNewPair: () => null  })
        store.onSubmit('wordToTranslate')
        expect(spy.mock.calls.length).toEqual(1)
    })

    // it('does have access to translated word after new word is submit', () => {
    //     const spy = jest.fn()
    //     spy.mockReturnValue(translationResponse)
    //     store = new TranslateWordStore({ api: { ...ApiTranslateWordMock, getTranslation: spy }, common: { getUserID: getUserID } })
    //     store.onSubmit('wordToTranslate')
    //     process.nextTick(() => expect(store.translatedWord).toEqual(translationResponse.text))
    // })

    // it('does add new pair in to words pairs list after new word is translated', () => {
    //     const spy = jest.fn()
    //     spy.mockReturnValueOnce({ ...translationResponse, text: 'dobrze' })
    //     store = new TranslateWordStore({ api: { ...ApiTranslateWordMock, getTranslation: spy }, common: { getUserID: getUserID } })
    //     store.onSubmit('good')
    //     process.nextTick(() => {
    //     })
    //     autorun(() => {
    //         const trigger = store.wordsPairs.processedList
    //     })
    //     process.nextTick(() => expect(store.wordsPairs.currentPair)
    //         .toEqual( {
    //             originWord: 'good',
    //             translatedWord: 'dobrze'}))
    // })
})
