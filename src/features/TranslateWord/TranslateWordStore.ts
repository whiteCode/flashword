import { action, observable, computed, reaction } from 'mobx'
import { IReactionDisposer } from 'mobx/lib/core/reaction'

import UserProfileStore from 'src/features/UserProfile/UserProfileStore'
import WordsPairs from './WordsPairs'
import { IWordsPairs, ITranslateWordApi } from './model'
import { IDefinition, IFrequency, IPronunciation, IDefinitionResponse, IApiError, IDetails, GetTranslation, GetPronunciation } from 'src/features/Api/model'
import TranslateWord from './TranslateWord'
import WordStore from './WordStore'

interface ITranslateWordStoreProps {
    api: ITranslateWordApi
    common: {
        getUserID: () => string | undefined,
    }
    onSaveNewPair: () => void
}

export default class TranslateWordStore {
    @observable word: WordStore // todo dedicated interface 
    @observable originWord: string
    @observable wordsPairs: WordsPairs
    reactions: IReactionDisposer[] = []
    @observable selectedCardIndex: number

    constructor(private props: ITranslateWordStoreProps) {
        const { saveWordsPairs } = props.api
        this.wordsPairs = new WordsPairs({
            api: {
                saveWordsPairs,
            },
            getUserID: this.props.common.getUserID,
            onSaveNewPair: this.props.onSaveNewPair,
        })
        const addNewWordReaction = reaction( // todo fix it
            () => this.word.translated.value,
            () => {
                if (this.word.translated.value.text) {
                    this.wordsPairs.onNewPairCreated({
                        originWord: this.originWord,
                        translatedWord: this.word.translated.value.text,
                    })
                }
            },
        )
        const saveDetailsReaction = reaction(() => this.details, () => {
            this.details && this.props.api.saveDetails({
                word: this.details,
            })
        }, { delay: 2000 })
        this.reactions.push(addNewWordReaction)
        this.reactions.push(saveDetailsReaction)
    }

    @computed get details(): IDetails | false {
        const { translated, definitions, pronunciation, examples, rhymes, synonyms, frequency } = this.word

        return this.word.isReady && {
            language: 'PL',
            origin: this.originWord,
            translated: translated.value.text,
            pronunciation: pronunciation.value.pronunciation.all,
            definitions: definitions.value.definitions.map(i => i.definition),
            examples: examples.value.examples,
            rhymes: rhymes.value.rhymes.all,
            synonyms: synonyms.value.synonyms,
            frequency: frequency.value.frequency,
        }
    }

    @action.bound
    onSubmit(origin: string): void {
        const { api } = this.props
        this.originWord = origin
        this.word = new WordStore({
            origin,
            api: {
                getTranslation: api.getTranslation,
                getPronunciation: api.getPronunciation,
                getFrequency: api.getFrequency,
                getDefinition: api.getDefinition,
                getExamples: api.getExamples,
                getSynonyms: api.getSynonyms,
                getRhymes: api.getRhymes,
            },
        })
    }

    @action.bound
    onClose() {
        // todo bind it
        this.reactions.forEach((reaction) => {
            reaction()
        })
    }
}
