import { promisedComputed } from 'computed-async-mobx'
import { computed } from 'mobx'
import { IWordApi } from './model'
import { GetPronunciation, GetTranslation, IFrequency, GetFrequency, IPronunciation, IDefinition, IExamplesResponse, ISynonymsResponse, IRhymes, GetExamples, GetDefinition, GetSynonyms, GetRhymes, IFrequencyResponse, IPronunciationResponse, IDefinitionResponse, IRhymesResponse } from 'src/features/Api/model'
import { frequency } from './factory'


export interface IWordDetail<T> { // TODO move it 
    value: T
    section: string
    isBusy: boolean
}

class Detail<T, Request extends { (request: { word: string }) }> implements IWordDetail<T> {  // TODO move it 
    section: string
    request = promisedComputed(this.params.defaultValue, async () => {
        const trigger = this.params.word
        return this.params.fetchFunction({ word: this.params.word })
            .catch(error => this.params.defaultValue)
    })

    constructor(private params:
        { section: string, word: string, defaultValue: T, fetchFunction: Request },
    ) {
        this.section = params.section
    }

    @computed get value(): T {
        return this.request.get()
    }

    @computed get isBusy(): boolean {
        return this.request.busy
    }
}

interface IWordStoreProps {
    origin: string
    api: IWordApi
}

export default class WordStore {
    origin: string
    translated: IWordDetail<{ text: string }>
    pronunciation: IWordDetail<IPronunciationResponse>
    frequency: IWordDetail<IFrequencyResponse>
    definitions: IWordDetail<IDefinitionResponse>
    examples: IWordDetail<IExamplesResponse>
    synonyms: IWordDetail<ISynonymsResponse>
    rhymes: IWordDetail<IRhymesResponse>
    @computed get isReady(): boolean  {
        return !this.translated.isBusy && !this.pronunciation.isBusy && !this.frequency.isBusy
            &&  !this.definitions.isBusy && !this.examples.isBusy && !this.synonyms.isBusy && !this.rhymes.isBusy
    }
    constructor(private props: IWordStoreProps) {
        this.origin = props.origin
        const { api } = props
        this.translated = new Detail<{ text: string }, GetTranslation>
            ({ section: 'Translated', word: this.props.origin, defaultValue: { text: '' }, fetchFunction: api.getTranslation })
        this.pronunciation = new Detail<IPronunciationResponse, GetPronunciation>
            ({ section: 'Pronunciation', word: this.props.origin, defaultValue: { pronunciation: { all: '' } }, fetchFunction: api.getPronunciation })
        this.frequency = new Detail<IFrequencyResponse, GetFrequency>
            ({ section: 'Frequency', word: this.props.origin, defaultValue: { frequency: { zipf: 0, perMillion: 0, diversity: 0 } }, fetchFunction: api.getFrequency })
        this.examples = new Detail<IExamplesResponse, GetExamples>
            ({ section: 'Examples', word: this.props.origin, defaultValue: { examples: [] }, fetchFunction: api.getExamples })
        this.definitions = new Detail<IDefinitionResponse, GetDefinition>
            ({ section: 'Definitions', word: this.props.origin, defaultValue: { definitions: [] }, fetchFunction: api.getDefinition })
        this.synonyms = new Detail<ISynonymsResponse, GetSynonyms>
            ({ section: 'Synonyms', word: this.props.origin, defaultValue: { synonyms: [] }, fetchFunction: api.getSynonyms })
        this.rhymes = new Detail<IRhymesResponse, GetRhymes>
            ({ section: 'Rhymes', word: this.props.origin, defaultValue: { rhymes: { all: [] } }, fetchFunction: api.getRhymes })
    }
}















