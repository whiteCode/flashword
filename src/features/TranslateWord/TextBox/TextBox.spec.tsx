import 'jest'
import { shallow } from 'enzyme'
import * as React from 'react'

import TextBox from './TextBox'

it('renders correctly', () => {
    const component = shallow(<TextBox onSubmit={() => null}/>)
    expect(component).toMatchSnapshot()
})
