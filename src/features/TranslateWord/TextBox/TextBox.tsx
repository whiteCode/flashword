import * as React from 'react'

import { ITextBox } from 'src/features/TranslateWord/model'
import styles from './styles'

export default class TextBox extends React.PureComponent<ITextBox> {
    textBox: HTMLInputElement

    onSubmitHandler(e: React.FormEvent<HTMLFormElement>): void {
        this.props.onSubmit(this.textBox.value)
        e.preventDefault()        
    }

    render() {
        return (
            <form onSubmit={(e) => this.onSubmitHandler(e)}>
                <input
                    className={styles.textBox}
                    type="text"
                    ref={(input: HTMLInputElement) => (this.textBox = input)}
                    placeholder="Type word to translate.."
                />
            </form>
        )
    }
}
