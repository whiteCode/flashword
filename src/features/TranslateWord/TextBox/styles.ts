import { style } from 'typestyle'
import { rem, percent, border, px } from 'csx'

import Colors from 'src/common/Colors'

const textBox = style({
    $debugName: 'textBox',
    padding: `${rem(1)} ${rem(1)} `,
    paddingLeft: rem(1),
    backgroundColor: 'inherit',
    width: `calc(${percent(100)} - ${rem(2)} - ${px(4)})`,
    border: border({ style: 'solid', width: px(2) }),
    borderRadius: rem(0.6),
    borderColor: Colors.violet1.toString(),
    marginTop: rem(0.5),
    fontFamily: 'Muli, sans-serif',    
    fontSize: rem(1),
    $nest: {
        '&:focus': {
            outline: 'none',
            borderColor: Colors.violet.toString(),
        },
    },
})

export default { textBox }
