import { style } from 'typestyle'
import { rem, px, percent } from 'csx'

const container = style({
    $debugName: 'container',
    flex: 5,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: percent(100),
})

const wrapper = style({
    $debugName: 'wrapper',
    flexGrow: 1,
    padding: rem(4),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
})

const black5 = 'rgba(15, 15, 0, 0.71)'

const responsesWrapper = style({
    $debugName: 'responseWrapper',
    fontFamily: 'Muli, sans-serif',            
    $nest: {
        'legend': {
            fontWeight: 600,
            color: black5,
            fontSize: rem(1.5),
            marginBottom: rem(0.8),         
        },
        'p': {
            fontWeight: 100,
            margin: rem(0),
            marginBottom: rem(1.2),   
            fontSize: rem(1.5),    
            color: black5,     
        },
    },
})

const swipeableContainer = {
    height: '100%',
    width: '100%',
}

const textWrapper = style({
    $debugName: 'textWrapper',
    alignItems: 'flex-end',
    justifyContent: 'end',
    textAlign: 'right',
    userSelect: 'none',
    overflow: 'hidden',
    height: `calc(${percent(100)} - ${rem(3)})`,
})

const textContainer = style({
    $debugName: 'container',
    color: 'rgba(15, 15, 0, 0.71)',
    padding: `${rem(0)} ${rem(1)}`,
    flexGrow: 1,
    alignItems: 'flex-start',
    justifyContent: 'normal',
    height: percent(95),
    $nest: {
        'header': {
            fontSize: rem(3),
            fontWeight: 400,
            lineHeight: rem(3.2),
            wordSpacing: rem(0.6),
            fontFamily: 'Muli, sans-serif',
            opacity: 0.8,
        },
        'p': {
            fontFamily: 'Playfair Display, serif',
            marginTop: rem(1.2),
            fontSize: rem(2),
            lineHeight: rem(2.2),
            textAlign: 'left',
            opacity: 0.6,
        },
    },
},
)

export default {
    container,
    wrapper,
    responsesWrapper,
    swipeableContainer,
    textContainer,
    textWrapper,
}
