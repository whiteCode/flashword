import { observable, computed } from 'mobx'

import { IWordsPairs, IWordsPairsApi } from './model'

export default class WordsPairs {
    @observable currentPair: IWordsPairs

    constructor(
        private props: {
            api: IWordsPairsApi,
            getUserID: () => string | undefined,
            onSaveNewPair: () => void,
        },
    ) { }

    onNewPairCreated(pair: IWordsPairs) {
        this.add(pair)
        this.save()
    }

    private save() {
        const id = this.props.getUserID()
        id && this.props.api.saveWordsPairs({ id, pair: this.currentPair }).then((response) => {
            this.props.onSaveNewPair()
        })
    }

    private add(pair: IWordsPairs) {
        this.currentPair = pair
    }
}
