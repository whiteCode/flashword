import { GetDefinition, GetPronunciation, GetFrequency, GetExamples,
    GetSynonyms, GetRhymes, GetTranslation, GetList, SaveWordPairs, SaveDetails } from 'src/features/Api/model'

export interface ITranslateWordApi {
    getDefinition: GetDefinition
    getPronunciation: GetPronunciation
    getFrequency: GetFrequency
    getExamples: GetExamples
    getSynonyms: GetSynonyms
    getRhymes: GetRhymes
    getTranslation: GetTranslation
    saveWordsPairs: SaveWordPairs
    saveDetails: SaveDetails
}

export interface IWordApi {
     getDefinition: GetDefinition
    getPronunciation: GetPronunciation
    getFrequency: GetFrequency
    getExamples: GetExamples
    getSynonyms: GetSynonyms
    getRhymes: GetRhymes
    getTranslation: GetTranslation
}

export interface IWordsPairsApi {
    saveWordsPairs: SaveWordPairs
}

export interface IWordsPairs {
    originWord: string
    translatedWord: string
}

export interface ITextBox {
    onSubmit: (value: string) => void
}

export interface ITranslationRequest {
    word: string
}

export interface ITranslationResponse {
    text: string
    from: {
        language: {
            didYouMean: boolean;
            iso: string; //todo enum
        };
        text: {
            didYouMean: boolean;
            autoCorrected: boolean;
            value: string;
        };
    }
    raw: string
}
