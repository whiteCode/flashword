import { getDeviceType, Device } from './model'
import { DeviceSpecific } from './DeviceSpecificLayout'

export { getDeviceType, Device, DeviceSpecific }
