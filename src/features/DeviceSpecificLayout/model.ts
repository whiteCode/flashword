export enum Device {
    Mobile = 0,
    Desktop = 1,
}
export const getDeviceType = (): Device => {
    return (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) 
        ? Device.Mobile 
        : Device.Desktop
}

