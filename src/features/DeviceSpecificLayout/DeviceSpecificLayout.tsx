import * as React from 'react'

import {Device} from './model'


export const DeviceSpecific = (MobileComponent, DesktopComponent, deviceType: Device) => class DS extends React.Component {
    render() {
        return (deviceType === Device.Mobile) 
            ? <MobileComponent {...this.props}/> 
            : <DesktopComponent {...this.props}/>        
    }
}



