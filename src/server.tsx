import * as path from 'path'
import * as express from 'express'

const normalizePort = (val: number | string): number | string | boolean => {
    const base = 10
    const port: number = typeof val === 'string' ? parseInt(val, base) : val
    return isNaN(port) ? val : port >= 0 ? port : false
}

const renderHtml = () =>
    `<html>
        <head>
            <meta charset="utf-8" />
            <title>FlashWords MVP</title>
            <link href="https://fonts.googleapis.com/css?family=Muli:200,400,700" rel="stylesheet">          
            <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
            <style>
            html, body, div, span, applet, object, iframe,
            h1, h2, h3, h4, h5, h6, p, blockquote, pre,
            a, abbr, acronym, address, big, cite, code,
            del, dfn, em, img, ins, kbd, q, s, samp,
            small, strike, strong, sub, sup, tt, var,
            b, u, i, center,
            dl, dt, dd, ol, ul, li,
            fieldset, form, label, legend,
            table, caption, tbody, tfoot, thead, tr, th, td,
            article, aside, canvas, details, embed, 
            figure, figcaption, footer, header, hgroup, 
            menu, nav, output, ruby, section, summary,
            time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 100%;
                font: inherit;
                vertical-align: baseline;
            }
            /* HTML5 display-role reset for older browsers */
            article, aside, details, figcaption, figure, 
            footer, header, hgroup, menu, nav, section {
                display: block;
            }
            body {
                line-height: 1;
            }
            ol, ul {
                list-style: none;
            }
            blockquote, q {
                quotes: none;
            }
            blockquote:before, blockquote:after,
            q:before, q:after {
                content: '';
                content: none;
            }
            table {
                border-collapse: collapse;
                border-spacing: 0;
            }
            </style>
            <style>
                .viewport {
                    width: 100%;
                    height: 100%;
                    margin: 0px!important;
                    overflow: hidden;
                }
        
                @media all and (min-width: 300px) {
                    html { 
                        font-size: 46px;
                    }
                }
        
                @media all and (min-width: 1000px) {
                    html { 
                        font-size: 16px;
                    }
                }
            </style>
        </head>    
        <body class="viewport">
            <div id="app" class="viewport"></div>
            <script src="/assets/bundle.js"></script>
        </body>
    </html>`

const defaultPort = 9966
const port = normalizePort(process.env.PORT || defaultPort)
const app = express()

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

app.use('/assets', express.static(path.join('assets'), { redirect: false }))

app.use((req: express.Request, res: express.Response) => {
    res.send(renderHtml())
})

app.listen(port, () => console.log(`App is listening on ${port}`))
