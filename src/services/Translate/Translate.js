const express = require('express')
const translate = require('google-translate-api');

const app = express()
const defaultPort = 8080;
const log = true;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/', async (req, res) => {
    if(log) {
        console.log("Translate service - request processing", req.query)
    }
    const response = await translate(req.query.word, { from: 'en', to: 'pl' }).then(res => {
         return res
    })
    res.send(response)
})

app.listen(defaultPort, () => console.log('Translate service listen on ' + defaultPort))

    // res.status(404).send("issue here")
