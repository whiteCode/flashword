import 'jest'

import RequestLimiter, { dailyRequestsLimit } from './RequestLimiter'
import RequestLimit from './RequestLimiter'

const RealDate = Date

const mockDate = (isoDate) => {
    //@ts-ignore
    global.Date = class extends RealDate {
        constructor() {
            super()
            return new RealDate(isoDate)
        }
    }
}

describe('request limiter basis', () => {
    let limiter: RequestLimiter
    const isoDateMock = '2018-03-11T11:48:08.148Z'
    const isoDateDayAfterMock = '2018-03-12T11:48:08.148Z'
    
    beforeEach(() => {
        mockDate(isoDateMock)
        limiter = new RequestLimiter()
    })

    afterEach(() => {
        global.Date = RealDate
    })

    it('initialization', () => {
        expect(limiter.currentDate).toBeDefined()
        expect(limiter.requestCount).toEqual(0)
        expect(limiter.limit).toEqual(dailyRequestsLimit)
    })

    it('does increase request count', () => {
        limiter.addRequest()
        expect(limiter.requestCount).toEqual(1)
    })

    it('does allow request with count below limit', () => {
        expect(limiter.isRequestAllowed()).toEqual(true)
    })

    it('does prohibits request with count above limit', () => {
        for (let i = 0; i < dailyRequestsLimit + 10; i++) {
            limiter.addRequest()
        }
        expect(limiter.isRequestAllowed()).toEqual(false)
    })

    it('does reset request limit on day change', () => {
        for (let i = 0; i < dailyRequestsLimit + 10; i++) {
            limiter.addRequest()
        }
        mockDate(isoDateDayAfterMock)        
        expect(limiter.isRequestAllowed()).toEqual(true)
    })
})


