import moment, {Moment} from 'moment'

export const dailyRequestsLimit = 1500

export default class RequestLimit {

    requestCount:number = 0
    limit: number = dailyRequestsLimit
    currentDate: number = moment().date()   

    isDateChanged() {
        return this.currentDate !== moment().date()
    }

    setCurrentDate() {
        this.currentDate = moment().date()
    }

    resetRequestCount() {
        this.requestCount = 0
    }

    isRequestAllowed() {
        if(this.isDateChanged()) {
            this.setCurrentDate()
            this.resetRequestCount()  
        }

        return this.requestCount < this.limit
    }

    addRequest() {
        this.requestCount++
    }

}
