import express from 'express'
import bodyParser from 'body-parser'
import unirest from 'unirest'
import RequestLimiter from './RequestLimiter'

const MongoClient = require('mongodb').MongoClient
const defaultPort = 8090
const app = express()

app.use(bodyParser())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE')
    next()
})

interface IUnirestHeaders {
    key: string
    host: string
}

class UnirestClient {
    limiter = new RequestLimiter()
    headers: IUnirestHeaders = {
        key: 'UNlZnpFWfNmshhRQ3lq9EY3NO0aip1Y6MTyjsn2VhGromkejjt',
        host: 'wordsapiv1.p.mashape.com',
    }
    baseUrl = 'https://wordsapiv1.p.mashape.com/words/'
    makeRequest(url: string) {
        return async (req, res) => {
            this.limiter.addRequest()

            return (this.limiter.isRequestAllowed())
                ? await unirest.get(this.baseUrl + req.query.word + '/'+url)
                    .header('X-Mashape-Key', this.headers.key)
                    .header('X-Mashape-Host', this.headers.host)
                    .end((result) => {
                        res.send(result.body) 
                    })
                : this.onProhibitedRequest(res)
        }
    }

    onProhibitedRequest(res) {
        res.status(402).send('The limit has been exceeded, please contact with product owners to extend your limit')
        res.send()
    }
}


MongoClient.connect('mongodb://mo1521_words:atqYs9aaxTJ5HSzd2BC5@85.194.242.213:27017/mo1521_words', (err, client) => {
    if (err) { return console.log(err) }
    const db = client.db('mo1521_words')
    const unirestClient = new UnirestClient()
    app.post('/wordsPairs', async (req, res) => {
        db.createCollection(req.body.id)
        await db.collection(req.body.id).insert(req.body.pair, (err, result) => {
            if (err) { 
                res.statusCode = 500
            }
            if(result) {
                res.statusCode = 200
            }
        })
        res.send()
    })

    app.post('/details', async (req, res) => {
        await db.collection('words').insertOne({ ...req.body.word, _id: req.body.word.origin }, (err, result) => {
            if (err) { 
                res.statusCode = 500
            }
            if(result) {
                res.statusCode = 200
            }
        })
        res.send()
    })

    app.get('/details', async (req, res) => {
        const word = await db.collection('words').find({ '_id': req.query.word }, (err, res) => {
            if(err) {
                res.status(404).send(`Can't find a resource`)
                return
            }
            if(res) {
                return res.toArray()
            }
        })
        res.send(word[0])
    })

    app.delete('/word', async (req, res) => {
        await db.collection(req.body.id.toString()).remove({originWord: req.body.word}, (err, res) => {
            if(err) {
                res.statusCode = 500
            }
            if(res) {
                res.statusCode = 200
            }
        })
        res.send()
    })

    app.get('/definition', unirestClient.makeRequest('definitions'))
    app.get('/pronunciation', unirestClient.makeRequest('pronunciation'))
    app.get('/frequency', unirestClient.makeRequest('frequency'))
    app.get('/examples', unirestClient.makeRequest('examples'))
    app.get('/synonyms', unirestClient.makeRequest('synonyms'))
    app.get('/rhymes', unirestClient.makeRequest('rhymes'))

    //change to header
    app.get('/wordsPairs', async (req, res) => {
        const doc = await db.collection(req.query.id).find().toArray()
        res.send({ list: doc })
    })

    app.listen(defaultPort, () => console.log('Backend service listen on ' + defaultPort))
})
