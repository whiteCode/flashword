import { color } from 'csx'

const violet = color('#5c3a58')
const violet1 = color('#352232')
const pink = color('#e6186d')
const yellow = color('#edb625')
const green = color('#49c4a1')
const blue = color('#81d2e0')
const white = color('#ffffff')

export default { violet, violet1, pink, yellow, green, blue, white }
