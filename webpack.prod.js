var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var urls = require('./src/config/urls.js')
let FaviconsWebpackPlugin = require('favicons-webpack-plugin')

module.exports = {
    context: __dirname,
    entry: path.resolve('src', 'index.tsx'),
    output: {
        filename: 'bundle.js',
        path: path.resolve('dist', 'assets')
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: ['babel-loader', 'awesome-typescript-loader'],
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
        alias: {
            'src': path.resolve('src/'),
        },
    },
    plugins: [
        new webpack.DefinePlugin({
            APIURL: JSON.stringify(urls[process.env.NODE_ENV]['api']),
            TRANSLATEURL: JSON.stringify(urls[process.env.NODE_ENV]['translate']) 
        }),
      
    ]
};

//         new ExtractTextPlugin(path.join('styles.css'))

// new FaviconsWebpackPlugin({
//     // Your source logo
//     logo: 'src/assets/favicon.png',
//     // The prefix for all image files (might be a folder or a name)
//     prefix: '',
//     // Emit all stats of the generated icons
//     emitStats: false,
//     // The name of the json containing all favicon information
//     statsFilename: 'iconstats-[hash].json',
//     // Generate a cache file with control hashes and
//     // don't rebuild the favicons until those hashes change
//     persistentCache: true,
//     // Inject the html into the html-webpack-plugin
//     inject: false,
//     // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
//     background: '#352232',
//     // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
//     title: 'FlashWord',

//     // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
//     icons: {
//       android: false,
//       appleIcon: false,
//       appleStartup: false,
//       coast: false,
//       favicons: true,
//       firefox: false,
//       opengraph: false,
//       twitter: false,
//       yandex: false,
//       windows: false
//     }})              