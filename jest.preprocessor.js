const tsc = require('typescript');
const babelJest = require('babel-jest');

module.exports = {
  process(src, path) {
    if (path.endsWith('.ts') || path.endsWith('.tsx')) {
      src = tsc.transpileModule(
        src,
        {
          compilerOptions: {
            'module': tsc.ModuleKind.ES2015,
            'target': tsc.ScriptTarget.ES2015,
            'moduleResolution': tsc.ModuleResolutionKind.NodeJs,
            'allowSyntheticDefaultImports': true,
            'jsx': tsc.JsxEmit.Preserve,
            'sourceMap': true,
            'baseUrl': './app',
          },
          fileName: path
        }
      );
      src = src.outputText;

      // update the path so babel can try and process the output
      path = path.substr(0, path.lastIndexOf('.')) + (path.endsWith('.ts') ? '.js' : '.jsx') || path;
    }

    if (path.endsWith('.js') || path.endsWith('.jsx')) {
      src = babelJest.process(src, path);
    }
    return src;
  },
};